const mapObj = require('./dhnodejsBundle.cjs');

const res = (...args) => {
  return mapObj.default(...args);
};

res.default = mapObj.default;
res.mapObjectSkip = mapObj.mapObjectSkip;

module.exports = res;
